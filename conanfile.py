from conans import ConanFile, CMake, tools

class BarConan(ConanFile):
    name = "Bar"
    version = "0.1"
    author = "Peter Lukacs lukacs.peter19@gmail.com"
    url = "https://gitlab.com/spektrof/conan-bar"
    description = "Bar library"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}
    generators = "cmake"

    def configure(self):
        if self.options.shared:
            self.options.fPIC=False

    def source(self):
        self.run("git clone https://gitlab.com/spektrof/conan-bar.git")

    def build(self):
        cmake = CMake(self)
        cmake.configure(source_folder="conan-bar")
        cmake.build()

    def package(self):
        self.copy("*.h", dst="include", src="conan-bar")
        self.copy("*.so", dst="lib", src="lib")
        self.copy("*.a", dst="lib", src="lib")

    def package_info(self):
        self.cpp_info.libdirs = ["lib"]
        self.cpp_info.libs = ["bar"]
        self.cpp_info.includedirs = ["include"]

